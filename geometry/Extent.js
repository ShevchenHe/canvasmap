/**
 * Created by esri-sgghe on 14-1-21.
 */
var Extent = function (a, b) {
    if (!a || a.length == 1) {
        return;
    }
    var points = b ? [a, b] : a;

}

Extent.prototype.calEnevlop=function (points) {
        var xs=[],
            ys=[];
        points.forEach(function(i){
            xs.push(i.x);
            ys.push(i.y);
        });
        this.min={};
        this.max={};
        this.min.x=Math.min.apply(Math,xs);
        this.max.x=Math.max.apply(Math,xs);
        this.min.y=Math.min.apply(Math,ys);
        this.max.y=Math.max.apply(Math,ys);
        return this;
    }
