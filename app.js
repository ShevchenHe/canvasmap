/**
 * Created by esri-sgghe on 14-1-21.
 */
var HQ = {};
HQ.Geometry = {};


HQ.util = {
    extend: function(superClass, subClass) {
        var F = function() {};
        F.prototype = superClass.prototype;
        subClass.prototype = new F();
        subClass.prototype.constructor = subClass;
        subClass.super = superClass;
        return subClass;
    },
    mixin: function(originalObject) {
        var mixinObjects = Array.prototype.slice.call(arguments, 1);
        var length = mixinObjects.length;
        for (var i = 0; i < length; i++) {
            var temp = mixinObjects[i];
            for (var j in temp) {
                originalObject[j] = temp[j];
            }
        }
        return originalObject;
    },
    isArray: function(o) {
        return Object.prototype.toString.call(o) === "[object Array]";
    },
    isFunction: function(o) {
        return Object.prototype.toString.call(o) === "[object Function]";
    },
    isObject: function(o) {
        return o != undefined && (o === null || this.isArray(o) || this.isFunction(0) || (typeof o) == 'object');
    },
    getMousePoint: function(e, map) { //获取鼠标的在map控件中的屏幕坐标，map的topleft为0，0
        var point = new HQ.Geometry.Point(e.clientX, e.clientY);
        point.x += document.body.scrollLeft + document.documentElement.scrollLeft;
        point.y += document.body.scrollTop + document.documentElement.scrollTop;

        for (var node = map._canvas; node; node = node.offsetParent) {
            point.x -= node.offsetLeft;
            point.y -= node.offsetTop;
        }
        return point;
    }
}


/*
 * use requestAnimationFrame to redarw map, below code get cross-browser requestAnimationFrame
 * */

HQ.requestAnimationFrame = (function() {
    return function(callback) {
        (window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame || function(callback) {
            setTimeout(callback, 1000 / 60)
        })(callback);
    }
})();
/*
 * Class EventUtil
 * */
HQ.EventUtil = {};
HQ.EventUtil.addEventListener = function(element, type, fn) {
    if (element.addEventListener) {
        element.addEventListener(type, fn, false);
    } else if (element.attachEvent) {
        element.attachEvent('on' + type, fn);
    } else {
        element['on' + type] = fn;
    };
};

HQ.EventUtil.removeEventListener = function(element, type, fn) {
    if (element.removeEventListener) {
        element.removeEventListener(type, fn, false);
    } else if (element.detachEvent) {
        element.detachEvent('on' + type, fn);
    } else {
        element['on' + type] = null;
    }
};

/*
 *
 * Cross Browser cancel event & stop bubbling
 * */
HQ.EventUtil.cancelEvent = function(e) {
    e.bubbles = true;
    e.cancelBubble = true;
    e.returnValue = false;
    if (e.stopPropagation) {
        e.stopPropagation();
    } else if (e.preventDefault) {
        e.preventDefault();
    }
    return false;
};

/*
 * Event&EventHandler Class
 * */
HQ.Event = {};

HQ.Event.DoubleClickHandler = function() {
    var handler = {
            id: "DoubleClickHandler"
        },
        map;

    function dbClickCallback(e) {
        var point = HQ.util.getMousePoint(e, map);
        map.zoomBy(e.shiftKey ? -1 : 1, point);
        HQ.EventUtil.cancelEvent(e);
    }

    handler.init = function(mapObject) {
        map = mapObject;
        HQ.EventUtil.addEventListener(map._canvas, 'dblclick', dbClickCallback);
        return handler;
    }
    handler.remove = function() {
        HQ.EventUtil.removeEventListener(map._canvas, 'dblclick', dbClickCallback);
    }
    return handler;
};

HQ.Event.MouseWheelHandler = function() {
    var handler = {
            id: 'MouseWheelHandler'
        },
        map;

    function mouseWheelCallback(e) {
        e.wheelDelta = e.wheelDelta ? e.wheelDelta : -e.detail;
        var point = HQ.util.getMousePoint(e, map);
        map.zoomBy(e.wheelDelta > 0 ? 1 : -1, point);
        HQ.EventUtil.cancelEvent(e);
    }

    handler.init = function(mapObject) {
        map = mapObject;
        HQ.EventUtil.addEventListener(map._canvas, 'DOMMouseScroll', mouseWheelCallback);
        HQ.EventUtil.addEventListener(map._canvas, 'mousewheel', mouseWheelCallback);
    }
    handler.remove = function() {
        HQ.EventUtil.removeEventListener(map._canvas, 'DOMMouseScroll', mouseWheelCallback);
        HQ.EventUtil.removeEventListener(map._canvas, 'mousewheel', mouseWheelCallback);
    }
    return handler;
};

HQ.Event.MouseDragHandler = function() {
    var handler = {
            id: 'MouseDragHandler'
        },
        map, prePoint;

    function mouseDown(e) {
        if (e.shiftKey || e.button == 2) {
            return;
        }
        HQ.EventUtil.addEventListener(map._canvas, 'mouseup', mouseUp);
        HQ.EventUtil.addEventListener(map._canvas, 'mousemove', mouseMove);
        prePoint = new HQ.Geometry.Point(e.clientX, e.clientY);
        map._canvas.style.cursor = 'move';

        return HQ.EventUtil.cancelEvent(e);
    }

    function mouseUp(e) {
        HQ.EventUtil.removeEventListener(map._canvas, 'mouseup', mouseUp);
        HQ.EventUtil.removeEventListener(map._canvas, 'mousemove', mouseMove);
        prePoint = null;
        map._canvas.style.cursor = '';
    }

    function mouseMove(e) {
        if (prePoint) {
            map.panBy(e.clientX - prePoint.x, e.clientY - prePoint.y);
            prePoint.x = e.clientX;
            prePoint.y = e.clientY;
        }
        return HQ.EventUtil.cancelEvent(e);
    }

    handler.init = function(mapObject) {
        map = mapObject;
        HQ.EventUtil.addEventListener(map._canvas, 'mousedown', mouseDown);
        return handler;
    }
    return handler;
};

//置于数组中，一起初始化
HQ.Event.MouseHandlers = function() {
    var handler = {
        id: 'MouseHandlers'
    };
    handler.init = function(mapObject) {
        handler.handlers = [HQ.Event.DoubleClickHandler().init(mapObject), HQ.Event.MouseDragHandler().init(mapObject), HQ.Event.MouseWheelHandler().init(mapObject)];
        return handler;
    }
    handler.remove = function() {
        for (var item in handler.handlers) {
            item.remove();
        }
        handler.handlers = [];
        return handler;
    }
    return handler;
};

/*
 * Class Callback manager--manager event callbacks
 *
 * */
HQ.CallbackManager = function(owner, events) {
    this.owner = owner;
    this.callbacks = {};
    for (var i = 0, length = events.length; i < length; i++) {
        this.callbacks[events[i]] = [];
    }
};

HQ.CallbackManager.prototype.addCallback = function(event, callback) {
    if (typeof(callback) == 'function' && this.callbacks[event]) {
        this.callbacks[event].push(callback);
    }
};

HQ.CallbackManager.prototype.removeCallback = function(event, callback) {
    if (typeof callback == 'function' && this.callbacks[event]) {
        var callbacks = this.callbacks[event];
        for (var i = callbacks.length; i >= 0; i--) {
            if (callbacks[i] === callback) {
                callbacks.splice(i, 1);
                break;
            }
        }
    }
};

HQ.CallbackManager.prototype.trigger = function(event, message) {
    if (this.callbacks[event]) {
        for (var i in this.callbacks[event]) {
            this.callbacks[event][i](this.owner, message);
        }
    }
};

/*
 * Implementation of Symbol
 * */
HQ.Symbol = function(style) {
    this.opacity = 0.8;
    this.strokeStyle = '#137CF3';
    this.fillStyle = '#EBF313';
    this.lineWidth = 5;
};

HQ.Symbol.SimpleMarkerSymbol = function(style) {
    this.size = 6;
    this.style = "CIRCLE"; //暂时准备支持CROSS SQUARE
    HQ.util.mixin(this, style);
};
HQ.Symbol.SimpleMarkerSymbol.prototype = new HQ.Symbol();
HQ.Symbol.SimpleMarkerSymbol.prototype.type = 'SimpleMarkerSymbol';


/*
 * SimpleLineSymbol
 * */
HQ.Symbol.SimpleLineSymbol = function(style) {
    HQ.util.mixin(this, style);
};

HQ.Symbol.SimpleLineSymbol.prototype = new HQ.Symbol();
HQ.Symbol.SimpleLineSymbol.prototype.type = "SimpleLineSymbol";

/*
 * SimpleFillSymbol
 * */
HQ.Symbol.SimpleFillSymbol = function(style) {
    HQ.util.mixin(this, style);
};

HQ.Symbol.SimpleFillSymbol.prototype = new HQ.Symbol();
HQ.Symbol.SimpleFillSymbol.prototype.type = "SimpleFillSymbol";

/*
 * Implementation of Geometry :Geomety
 * is the base class of point,polyline,polygon
 * */

HQ.Geometry.Geometry = function() {

};

HQ.Geometry.Geometry.prototype.mapPointTocanvasPoint = function(map, point) {
    var x = (point.x - map.canvasOrigin.x) / map.resolution;
    var y = (map.canvasOrigin.y - point.y) / map.resolution;
    return new HQ.Geometry.Point(x, y);
};

/*
 * Implementation of Geometry:Point
 * */

HQ.Geometry.Point = function(x, y) {
    this.x = x;
    this.y = y;
};
HQ.Geometry.Point.prototype = new HQ.Geometry.Geometry();

HQ.Geometry.Point.prototype.type = "Point";

HQ.Geometry.Point.prototype._draw = function(map, symbol) {
    var self = this;
    var context = map._context;
    var canvasPoint = this.mapPointTocanvasPoint(map, self);
    if (symbol.type === "SimpleMarkerSymbol") {
        if (symbol.style === "CIRCLE") {
            context.strokeStyle = symbol.strokeStyle;
            context.fillStyle = symbol.fillStyle;
            context.beginPath();
            context.arc(canvasPoint.x, canvasPoint.y, symbol.size, 0, Math.PI * 2, false);
            context.stroke();
            context.fill();
        }
    }
};



/*
 * Implentation of Geometry:Extent
 * */
HQ.Geometry.Extent = function(a, b) {
    if (!a || a.length == 1) {
        return;
    }
    var points = b ? [a, b] : a;
    this.calEnevlop(points);
};

HQ.Geometry.Extent.prototype = new HQ.Geometry.Geometry();

HQ.Geometry.Extent.prototype.calEnevlop = function(points) {
    var xs = [],
        ys = [];
    points.forEach(function(i) {
        xs.push(i.x);
        ys.push(i.y);
    });
    this.min = {};
    this.max = {};
    this.min.x = Math.min.apply(Math, xs);
    this.max.x = Math.max.apply(Math, xs);
    this.min.y = Math.min.apply(Math, ys);
    this.max.y = Math.max.apply(Math, ys);
    return this;
};


/*
 * Implentation of Geometry:Polyline
 * */

HQ.Geometry.Polyline = function(points) {
    this.points = points;

};

HQ.Geometry.Polyline.prototype = new HQ.Geometry.Geometry();
HQ.Geometry.Polyline.prototype.type = "Polyline";

HQ.Geometry.Polyline.prototype._draw = function(map, symbol) {
    var self = this,
        context = map._context;
    this._path = this.points.map(self.mapPointTocanvasPoint.bind(self, map));
    var length = this._path.length;
    context.strokeStyle = symbol.strokeStyle;
    context.fillStyle = symbol.fillStyle;
    context.beginPath();
    context.moveTo(this._path[0].x, this._path[0].y);
    for (var i = 1; i < length; ++i) {
        var currentpoint = this._path[i];
        context.lineTo(currentpoint.x, currentpoint.y);
    }
    context.fill();
    context.stroke();


};

/*
 * Implentation of Geometry:Polygon
 * */
HQ.Geometry.Polygon = function(points) {
    if (points.length < 4) {
        throw new TypeError('Constructor of Polygon needs 3 points at least')
    } else {
        if (JSON.stringify(points[0]) != JSON.stringify(points[points.length - 1])) {
            throw new TypeError('The first point and the Last point is not the same')
        } else {
            this.points = points;
        }
    }
};

HQ.Geometry.Polygon.prototype = new HQ.Geometry.Geometry();
HQ.Geometry.Polygon.prototype.type = "Polygon";

HQ.Geometry.Polygon.prototype._draw = function(map, symbol) {
    var self = this,
        context = map._context;
    this._path = this.points.map(self.mapPointTocanvasPoint.bind(self, map));
    var length = this._path.length;
    context.strokeStyle = symbol.strokeStyle;
    context.fillStyle = symbol.fillStyle;
    context.beginPath();
    context.moveTo(this._path[0].x, this._path[0].y);
    for (var i = 1; i < length - 1; ++i) {
        var currentpoint = this._path[i];
        context.lineTo(currentpoint.x, currentpoint.y);
    }
    context.closePath();
    context.fill();
    context.stroke();
};



/*
 * Implementation of Graphic
 * */
HQ.Graphic = function(geometry, symbol, attributes) {
    this.geometry = geometry;
    if (symbol) {
        this.symbol = symbol;
    } else if (this.geometry.type === 'Point') {
        this.symbol = new HQ.Symbol.SimpleMarkerSymbol();
    } else if (this.geometry.type === 'Polyline') {
        this.symbol = new HQ.Symbol.SimpleLineSymbol();
    } else if (this.geometry.type === 'Polygon') {
        this.symbol = new HQ.Symbol.SimpleFillSymbol();
    }
    //this.symbol=symbol?symbol:new HQ.Symbol.SimpleMarkerSymbol();
    this.attributes = attributes;
    this._graphicsLayer = null;
};

HQ.Graphic.prototype.getLayer = function() {
    return this._graphicsLayer;
};

HQ.Graphic.prototype._draw = function(map) {
    this.geometry._draw(map, this.symbol);
};



/*
 * Implementation Of Map Object
 * opitons中暂时重要的参数就是extent,如果初始化的时候没有设置
 * */
HQ.Map = function(id, options) {
    this.basemap = null;
    this.extent = null;
    this.resolutions = null;
    this.resolution = null;
    this.canvasOrigin = null; //coordinate of canvas topleft in map unit
    HQ.util.mixin(this, options);
    this._canvas = document.getElementById(id);
    this.width = this._canvas.width;
    this.height = this._canvas.height;
    this._context = this._canvas.getContext('2d');
    this.layers = [];
    this._graphicsLayers = [];
    this.init();
};

HQ.Map.prototype.init = function() {
    this._initEventHandlers();
    this._initCallbackManager();
    if (this.extent && this.resolutions) {
        this._initResolutionAndCanvasOrigin();
    }
};

HQ.Map.prototype.getGraphicsLayers = function() {
    return this._graphicsLayers;
}

HQ.Map.prototype._initEventHandlers = function() {
    this.eventHandlers = new HQ.Event.MouseHandlers().init(this);
};

HQ.Map.prototype._initCallbackManager = function() {
    //init map event callback manager
    this.callbackManager = new HQ.CallbackManager(this, ['zoomEnd', 'panEnd', 'centerEnd', 'extentChange']);
};

HQ.Map.prototype.addLayer = function(layer, index) {
    //检测layer类型

    if (arguments.length == 1) {
        this.layers.push(layer);
        if (layer instanceof HQ.Layer.GraphicsLayer) {
            this._graphicsLayers.push(layer);
        }
    } else if (arguments.length == 2) {
        var length = this.layers.length;
        if (index > length) {
            this.layers.push(layer);
        } else {
            if (index > 0) {
                this.layers.splice(index, 0, layer);
            } else {
                this.layers.splice(0, 0, layer);
            }
        }
        //index>length : this.layers.push(layer) ? (index > 0 ? this.layers.splice(index, 0, layer) : this.layers.splice(0, 0, layer));
    }
    layer._map = this;
    if (this.basemap) {
        this.addToMap(layer);
    } else {
        //将第一个加载的图层的resolutions作为整个map控件的resolutions,如果初始化的时候没有extent，将第一个图层的fullExtent作为inintExtent
        this.basemap = layer;

        if (!this.extent) {

            this.extent = new HQ.Geometry.Extent(new HQ.Geometry.Point(layer.tileInfo.fullExtent.min.x, layer.tileInfo.fullExtent.min.y), new HQ.Geometry.Point(layer.tileInfo.fullExtent.max.x, layer.tileInfo.fullExtent.max.y))

        }
        if (!this.resolutions) {
            this.resolutions = layer.tileInfo.resolutions;
        }
        if (this.extent && this.resolutions) {
            this._initResolutionAndCanvasOrigin();
        }
        this.addToMap(layer);
        //this.lods=layer.lods;
    }
};


HQ.Map.prototype._initResolutionAndCanvasOrigin = function() {
    var resolutions = this.resolutions;
    this.canvasOrigin = new HQ.Geometry.Point(this.extent.min.x, this.extent.max.y);
    this.resolution = (this.extent.max.x - this.extent.min.x) / (this.width);
    for (var i = 0, length = this.resolutions.length; i < length; i++) {
        if (this.resolution >= resolutions[i]) {
            this.resolution = resolutions[i];
            this.zoom = i;
            break;
        }
    }
    //判断canvas是否可以容下当前extent的地图，如果不能则要修正extent
    if ((this.extent.max.x - this.extent.min.x) / this.resolution > this.width) {
        this.extent.max.x = this.extent.min.x + this.resolution * this.width;
        this.extent.min.y = this.extent.max.y - this.resolution * this.height;
    }
};

HQ.Map.prototype.removeLayer = function(layer) {
    layer.remove();
    return this;
};

HQ.Map.prototype.addToMap = function(layer) {
    layer.addTo(this);
    return this;
};

HQ.Map.prototype.draw = function() {
    this._context.clearRect(0, 0, this.width, this.height);
    for (var i in this.layers) {
        this.layers[i]._draw();
    }
};

//create a closure for requestAnimationFrame
HQ.Map.prototype.getRedraw = function() {
    var self = this;
    if (!this._redraw) {
        this._redraw = function() {
            self.draw();
        }
    }
    return this._redraw;
};

HQ.Map.prototype.getAllLayerUpdate = function() {
    for (var i in this.layers) {
        this.layers[i].callbackManagers.trigger('update');
    }
};

HQ.Map.prototype.zoomBy = function(offset, point) {
    var preResolution = this.resolution,
        self = this;
    var mapPoint = new HQ.Geometry.Point(this.canvasOrigin.x + point.x * preResolution, this.canvasOrigin.y - point.y * preResolution);
    if (offset > 0) {
        this.resolution = this.resolutions[this.zoom + 1];
        this.zoom += 1;
        if (this.zoom > this.resolutions.length - 1) {
            this.zoom = this.resolutions.length - 1;
        }

    } else {
        this.resolution = this.resolutions[this.zoom - 1];
        this.zoom -= 1;
        if (this.zoom < 0) {
            this.zoom = 0;
        }
    }
    this.canvasOrigin.x = mapPoint.x - this.resolution * point.x;
    this.canvasOrigin.y = mapPoint.y + this.resolution * point.y;

    this.extent.min.x = this.canvasOrigin.x;
    this.extent.max.y = this.canvasOrigin.y;
    this.extent.max.x = this.canvasOrigin.x + this.width * this.resolution;
    this.extent.min.y = this.canvasOrigin.y - this.height * this.resolution;
    this.getAllLayerUpdate();
    HQ.requestAnimationFrame(self.getRedraw());
};

HQ.Map.prototype.panBy = function(dx, dy) {
    var self = this;
    this.canvasOrigin.x -= dx * this.resolution;
    this.canvasOrigin.y += dy * this.resolution;

    this.extent.min.x = this.canvasOrigin.x;
    this.extent.max.y = this.canvasOrigin.y;
    this.extent.max.x = this.canvasOrigin.x + this.width * this.resolution;
    this.extent.min.y = this.canvasOrigin.y - this.height * this.resolution;
    this.getAllLayerUpdate();
    HQ.requestAnimationFrame(self.getRedraw());
};



/*
 * Class Lyaer
 *
 * */

HQ.Layer = function() {
    //

};

HQ.Layer.prototype.addTo = function(map) {
    this.init();
    this._map.lods = this.lods;
    //var map = this._map;
    //map.draw();
    HQ.requestAnimationFrame(map.getRedraw());
};

HQ.Layer.prototype.remove = function() {};


/*
 * Class  TileInfo
 * */
HQ.Layer.TileInfo = function(width, height, dpi, originPoints, resolutions) {
    //默认是web莫卡托，切图方案是google maps，bing map，arcgisonline的切图方案
    HQ.util.mixin(this, {
        width: 256,
        height: 256,
        dpi: 96,
        fullExtent: new HQ.Geometry.Extent(new HQ.Geometry.Point(-2.0037508342787E7, -2.0037508342787E7), new HQ.Geometry.Point(2.0037508342787E7, 2.0037508342787E7)),
        origin: new HQ.Geometry.Point(-2.0037508342787E7, 2.0037508342787E7),
        resolutions: [156543.033928, 78271.51696402031, 39135.758482010155, 19567.879241005077, 9783.939620502539, 4891.969810251269, 2445.9849051256347, 1222.9924525628173, 611.4962262814087, 305.74811314070433, 152.87405657035217, 76.43702828517608, 38.21851414258804, 19.10925707129402, 9.55462853564701, 4.777314267823505, 2.3886571339117526, 1.1943285669558763, 0.5971642834779382, 0.2985821417389691, 0.14929107086948454, 0.07464553543474227, 0.037322767717371134]
    });
    if (arguments.length == 1 && HQ.util.isObject(width)) {
        HQ.util.mixin(this, width);
    } else if (arguments.length == 2) {
        HQ.util.mixin(this.tileInfo, {
            width: arguments[0],
            height: arguments[1]
        })
    } else if (arguments.length == 3) {
        HQ.util.mixin(this, {
            width: arguments[0],
            height: arguments[1],
            dpi: arguments[2]
        });
    } else if (arguments.length == 5) {
        HQ.util.mixin(this, {
            width: arguments[0],
            height: arguments[1],
            dpi: arguments[2],
            origin: arguments[3],
            resolutions: arguments[4]
        });
    }
};

/*
 * Class tile
 * */
HQ.Layer.tile = function(url, layer, mapleft, maptop) {
    this._layer = layer;
    this.mapTop = maptop;
    this.mapLeft = mapleft;
    this.image = new Image(); //host Object only work in browser
    this.image.src = url;
    this.image.loaded = false;
    this.callbackManager = new HQ.CallbackManager(this, ['draw']);
    var self = this;
    this.image.onload = function() {
        self.load.call(self);
    }

};

HQ.Layer.tile.prototype.load = function() {
    this.image.loaded = true;
    this.calPosition();
};

HQ.Layer.tile.prototype.calPosition = function() {
    var map = this._layer._map;
    var mapOrigin = map.canvasOrigin;
    this.canvasLeft = ((this.mapLeft - mapOrigin.x) / map.resolution + 0.5) | 0;
    this.canvasTop = ((mapOrigin.y - this.mapTop) / map.resolution + 0.5) | 0;
    if (this.draw)
        this.callbackManager.trigger('draw');
};

/*
    Implentation of vcetor tile
 */

HQ.Layer.vectorTile = function(dataUrl, layer, mapleft, maptop) {
    var self = this;
    this.dataUrl = dataUrl;
    this._layer = layer;
    this.features = null;
    this.dataLoaded = false;
    this.mapTop = maptop;
    this.mapLeft = mapleft;
    var req = new XMLHttpRequest();
    req.onreadystatechange = self.getVectorDataCompleted(req, self);
    req.open("GET", dataUrl, true);
    req.send();
};

HQ.Layer.vectorTile.prototype.calPosition = function() {
    var map = this._layer._map;
    var mapOrigin = map.canvasOrigin;
    // refer: http://www.html5rocks.com/zh/tutorials/canvas/performance/#toc-avoid-float
    this.canvasLeft = ((this.mapLeft - mapOrigin.x) / map.resolution + 0.5) | 0;
    this.canvasTop = ((mapOrigin.y - this.mapTop) / map.resolution + 0.5) | 0;
    //if (this.draw)
    //    this.callbackManager.trigger('draw');
};

HQ.Layer.vectorTile.prototype.getVectorDataCompleted = function(req, vectortile) {
    if (req.readyState != 4) {
        return;
    }
    if (req.status == 200 || req.status == 304) {
        //
        vectorTile.features = JSON.parse(req.responseText).features;
        vectorTile.dataLoaded = true;
        vectorTile.calPosition();
    }
};



/*
 * Class TiledLayer
 * init:初始化图层
 *initLodsVisibleRectInfo:计算每个级别中的起始tile编号
 *getCurrentRectTilesRange:计算当前地图范围内的切片起始编号
 *
 * */

/**
 *
 * @param {string} url
 * @param {object} tileInfo
 * @constructor of Class TiledLayer
 */
HQ.Layer.TiledLayer = function(url, tileInfo) {
    this.lods = [];
    this.serviceUrl = url;
    this.tileInfo = tileInfo;
    this.tiles = [];
    this.callbackManagers = new HQ.CallbackManager(this, ['update']);
    //this.init();
};

HQ.Layer.TiledLayer.prototype = new HQ.Layer();

HQ.Layer.TiledLayer.prototype.init = function() {
    this.initLodsVisibleRectInfo();
    this.getCurrentRectTilesRange();
    this.getTiles();
    //Layer update event
    this.callbackManagers.addCallback('update', function(layer, message) {
        layer.getCurrentRectTilesRange();
        layer.getTiles();
    })
};

HQ.Layer.TiledLayer.prototype._draw = function() {
    var map = this._map,
        context = map._context,
        tiles = this.tiles,
        width = this.tileInfo.width,
        height = this.tileInfo.height,
        drawendtilenumber = 0,
        length = tiles.length;
    var drawImage = function(tile) {
            context.drawImage(tile.image, 0, 0, width, height, tile.canvasLeft, tile.canvasTop, width, height);
            drawendtilenumber += 1;
            //确定basemap的切片load完毕后，再更新graphic layer
            if (drawendtilenumber == length) {
                console.log('All tiles have been drawn');
                var gls = map.getGraphicsLayers();
                for (var i = 0, glslength = gls.length; i < glslength; ++i) {
                    gls[i].callbackManagers.trigger('update');
                }
            }
        }
        //console.log(length);
    for (var i = 0; i < length; i++) {
        var tile = tiles[i],
            image = tile.image;
        //console.log(image.loaded);
        if (image.loaded) {
            drawImage(tile);
        } else {
            tile.draw = true;
            tile.callbackManager.addCallback('draw', drawImage);
        }
    }
};

HQ.Layer.TiledLayer.prototype.initLodsVisibleRectInfo = function() {
    var tileInfo = this.tileInfo;
    var layerResolutions = tileInfo.resolutions,
        length = layerResolutions.length,
        fullExtent = tileInfo.fullExtent,
        width = tileInfo.width,
        height = tileInfo.height;

    var origin = tileInfo.origin;
    for (var i = 0; i < length; i++) {
        var temp = {};
        temp.resolution = layerResolutions[i];
        temp.startTileRow = Math.floor((origin.y - fullExtent.max.y) / height / layerResolutions[i]);
        temp.startTileCol = Math.floor((fullExtent.min.x - origin.x) / width / layerResolutions[i]);
        temp.endTileRow = Math.floor((origin.y - fullExtent.min.y) / height / layerResolutions[i]);
        temp.endTileCol = Math.floor((fullExtent.max.x - origin.x) / width / layerResolutions[i]);
        if (temp.startTileRow < 0) {
            temp.startTileRow = 0
        }
        if (temp.startTileCol < 0) {
            temp.startTileCol = 0
        }
        this.lods.push(temp);
    }

};

HQ.Layer.TiledLayer.prototype.getCurrentRectTilesRange = function() {
    var map = this._map,
        zoom = map.zoom,
        tileInfo = this.tileInfo,
        extent = map.extent,
        resolution = map.resolution,
        temp = {},
        width = tileInfo.width,
        height = tileInfo.height,
        origin = tileInfo.origin;

    /*    for (var i in this.resolutions) {
     if (resolution >= this.resolutions[i]) {
     resolution = this.resolutions[i];
     temp.z = i;
     break;
     }
     }*/
    //map.zoom = temp.z;
    //map.resolution = resolution;
    temp.z = zoom;
    temp.startTileRow = Math.floor((origin.y - extent.max.y) / height / resolution);
    temp.startTileCol = Math.floor((extent.min.x - origin.x) / width / resolution);
    temp.endTileRow = Math.floor((origin.y - extent.min.y) / height / resolution);
    temp.endTileCol = Math.floor((extent.max.x - origin.x) / width / resolution);

    temp.startTileCol = temp.startTileCol < this.lods[zoom].startTileCol ? this.lods[zoom].startTileCol : temp.startTileCol;
    temp.startTileRow = temp.startTileRow < this.lods[zoom].startTileRow ? this.lods[zoom].startTileRow : temp.startTileRow;
    temp.endTileCol = temp.endTileCol > this.lods[zoom].endTileCol ? this.lods[zoom].endTileCol : temp.endTileCol;
    temp.endTileRow = temp.endTileRow > this.lods[zoom].endTileRow ? this.lods[zoom].endTileRow : temp.endTileRow;

    this._currentRectTilesRange = temp;
};

/*
 * Implentation of Class Gistech Maps
 * http://map.geoq.cn/ArcGIS/rest/services/ChinaOnlineCommunity/MapServer/tile/z/x/y
 *
 * */

/**
 * constructor of GisTechMaps
 * @param {string} url      service url
 * @param {Object} tileInfo tileInfo
 */
HQ.Layer.GisTechMaps = function(url, tileInfo) {
    this.serviceUrl = url;
    this.callbackManagers.owner = this;
    if (tileInfo == undefined) {
        this.tileInfo = new HQ.Layer.TileInfo();
    } else {
        this.tileInfo = tileInfo;
    }

};

HQ.Layer.GisTechMaps.prototype = new HQ.Layer.TiledLayer();

HQ.Layer.GisTechMaps.prototype.getTiles = function() {
    var self = this;
    var currentRectTilesrange = this._currentRectTilesRange;
    this.tiles = [];
    var urlPrefix = this.serviceUrl + "/tile/" + currentRectTilesrange.z + "/";
    for (var i = currentRectTilesrange.startTileRow; i <= currentRectTilesrange.endTileRow; i++) {
        for (var j = currentRectTilesrange.startTileCol; j <= currentRectTilesrange.endTileCol; j++) {
            var tileUrl = urlPrefix + i + "/" + j,
                mapLeft = this.tileInfo.origin.x + j * this.tileInfo.width * this._map.resolution,
                mapTop = this.tileInfo.origin.y - i * this.tileInfo.height * this._map.resolution;
            var tile = new HQ.Layer.tile(tileUrl, self, mapLeft, mapTop);
            this.tiles.push(tile);
        }
    }
};

/*
 * Implementation of graphicsLayer
 * subtype of HQ.Layer
 * */

HQ.Layer.GraphicsLayer = function() {
    var self = this;
    this._map = false;
    this.graphics = [];
    //    this.callbackManagers = new HQ.CallbackManager(this, ['update', 'GraphicAdd']);
    //    this.callbackManagers.addCallback('update', self._getDraw());

};

HQ.Layer.GraphicsLayer.prototype = new HQ.Layer();

HQ.Layer.GraphicsLayer.prototype.init = function() {
    var self = this;
    this.callbackManagers = new HQ.CallbackManager(this, ['update', 'GraphicAdd']);
    this.callbackManagers.addCallback('update', self._getDraw());
}

HQ.Layer.GraphicsLayer.prototype.add = function(graphic) {
    graphic._graphicsLayer = this;
    this.graphics.push(graphic);
    if (this._map) {
        this.callbackManagers.trigger('update');
    } else {
        return;
    }
};

HQ.Layer.GraphicsLayer.prototype.clear = function() {
    this.graphics = [];
    this.callbackManagers.trigger('update');
};

HQ.Layer.GraphicsLayer.prototype.remove = function(graphic) {
    var length = this.graphics.length;
    //    while (length--) {
    //        if (this.graphics.splice(length, 1));
    //    }
    if (this._map)
        this.callbackManagers.trigger('update');
};

HQ.Layer.GraphicsLayer.prototype._draw = function() {
    var length = this.graphics.length;
    for (var i = 0; i < length; i++) {
        this.graphics[i]._draw(this._map);
    }
};

HQ.Layer.GraphicsLayer.prototype._getDraw = function() {
    var length = this.graphics.length,
        graphics = this.graphics,
        map = this._map;
    var draw = function() {
        for (var i = 0; i < length; i++) {
            graphics[i]._draw(map);
        }
    }
    return draw;
};



/*
    Implentation of VectorTileLayer
    now,only support json
    the url template is just like "http://tile.openstreetmap.us/vectiles-land-usages/12/656/1582.json"
    OSM also supply binary format *.mvt, just like "http://tile.openstreetmap.us/vectiles-land-usages/{z}/{x}/{y}.mvt"
 */


/**
 * VectorTileLayer constructor
 * @param {string} url      service url
 * @param {Object} tileInfo tileInfos
 * @param {string} type     vector format .json .mvt .geojson
 */
HQ.Layer.VectorTileLayer = function(url, tileInfo, type /*json geojson mvt..*/ ) {
    this.serviceUrl = url;
    this.type = type;
    this.callbackManagers.owner = this;
    //初始化一个renderer，渲染json用
    this._renderer = new HQ.Renderer.CanvasRenderer();
    if (tileInfo == undefined) {
        //默认3857
        this.tileInfo = new HQ.Layer.TileInfo();
    } else {
        this.tileInfo = tileInfo;
    }
};

/*
Inherit form HQ.Layer.Tiledlayer
 */

HQ.Layer.VectorTileLayer.prototype = new HQ.Layer.Tiledlayer();


/**
 * Override getTies
 * @return {[type]} [description]
 */
HQ.Layer.VectorTileLayer.prototype.getTiles = function() {
    var self = this;
    var currentRectTilesrange = this._currentRectTilesRange;
    this.tiles = [];
    var urlPrefix = this.serviceUrl + "/" + currentRectTilesrange.z + "/";
    for (var i = currentRectTilesrange.startTileRow; i <= currentRectTilesrange.endTileRow; i++) {
        for (var j = currentRectTilesrange.startTileCol; j <= currentRectTilesrange.endTileCol; j++) {
            var tileUrl = urlPrefix + i + "/" + j + "." + this.type,
                mapLeft = this.tileInfo.origin.x + j * this.tileInfo.width * this._map.resolution,
                mapTop = this.tileInfo.origin.y - i * this.tileInfo.height * this._map.resolution;
            var tile = new HQ.Layer.tile(tileUrl, self, mapLeft, mapTop);
            this.tiles.push(tile);
        }
    }
};

/*
    Override _draw method
 */

HQ.Layer.VectorTileLayer.prototype._draw = function() {
    var map = this._map,
        context = map._context,
        tiles = this.tiles,
        width = this.tileInfo.width,
        height = this.tileInfo.height,
        drawendtilenumber = 0,
        length = tiles.length;
    var drawImage = function(tile) {
            context.drawImage(tile.image, 0, 0, width, height, tile.canvasLeft, tile.canvasTop, width, height);
            drawendtilenumber += 1;
            //确定basemap的切片load完毕后，再更新graphic layer
            if (drawendtilenumber == length) {
                console.log('All tiles have been drawn');
                var gls = map.getGraphicsLayers();
                for (var i = 0, glslength = gls.length; i < glslength; ++i) {
                    gls[i].callbackManagers.trigger('update');
                }
            }
        }
        //console.log(length);
    for (var i = 0; i < length; i++) {
        var tile = tiles[i],
            image = tile.image;
        //console.log(image.loaded);
        if (image.loaded) {
            drawImage(tile);
        } else {
            tile.draw = true;
            tile.callbackManager.addCallback('draw', drawImage);
        }
    }


}



/*
Implentation of GeoJson Renderer

 */

HQ.Renderer.CanvasRenderer = function() {

};

//Renderer Point
HQ.Renderer.CanvasRenderer.prototype.renderPoint = function(point, style, context) {

};