/**
 * Created by esri-sgghe on 14-1-21.
 */

/*
 * options:{extent:}
 *
 * */
var Map = function (id, options) {
    Util.mixin(this, options);
    this._canvas = document.getElementById(id);
    this._context = this._canvas.getContext('2d');
    this.layers = [];
    //this.init();
}

Map.prototype.init = function () {
    //this._initEvents();
}
Map.prototype.addLayer = function (layer, index) {
    //检测layer类型
    if (arguments.length == 1) {
        this.layers.push(layer);
    } else if (arguments.length == 2) {
        var length = this.layers.length;
//        (index > length)
//    :
//        this.layers.push(layer) ? (index > 0 ? this.layers.splice(index, 0, layer) : this.layers.splice(0, 0, layer));
    }
    layer._map = this;
    this.addToMap(layer);
}

Map.prototype.removeLayer=function(layer){
    layer.remove();
    return this;
}

Map.prototype.addToMap = function (layer) {
    layer.addTo(this);
    return this;
}