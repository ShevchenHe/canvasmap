/**
 * Created by esri-sgghe on 14-1-21.
 */
var util = {
    extend: function (superClass, subClass) {
        var F = function () {
        };
        F.prototype = superClass.prototype;
        subClass.prototype = new F();
        subClass.prototype.constructor = subClass;
        subClass.super = superClass;
        return subClass;
    },
    mixin: function (originalObject) {
        var mixinObjects = Array.prototype.slice.call(arguments, 1);
        var length = mixinObjects.length;
        for (var i = 0; i < length; i++) {
            var temp = mixinObjects[i]
            for (var j in temp) {
                originalObject[j] = temp[j];
            }
        }
        return originalObject;
    },
    isArray: function (o) {
        return Object.prototype.toString.call(o) === "[object Array]";
    },
    isFunction: function (o) {
        return Object.prototype.toString.call(o) === "[object Function]";
    },
    isObject: function (o) {
        return o != undefined && (o === null || this.isArray(o) ||this.isFunction(0)||(typeof o)=='object' );
    }
}